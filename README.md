# Lunar Fashion Project

Repo: https://bitbucket.org/ashainrox/lunar_fashion/src/master/

---
## Technology Stack
- PHP 8.0
    - laravel V8.54 => Framework
        - Laravel Sanctum => For Authentication
    
- Sqlite3 => For data storage (light weight)




---
## Docker Installation Dependencies:
1> Install docker

2>Clone the project

3>Run the below commands.
```
cd {{project path}}

#setup the ENV file.
cp [ .env.dev.docker | .env.production.docker ] .env


[nano | vim] .env
# Configure the `APP_URL` value, with the domain you are going to host the app.
#       Ex: APP_URL="https://lunar-fashion.vestiairecollective.com" 
# Next, configure exposure port `DOCKER_HOST_OUT_PORT` to 80 or 443

docker-compose up -d --build

./php-exec.sh
    chgrp -R www-data storage bootstrap/cache
    chmod -R ug+rwx  storage bootstrap/cache

    #install vendors
    php composer.phar install
    
    
    #Populates the tables.
    php artisan migrate
    
    #if you need to run a quick test, run the below, it creates a additional testing database.
    php artisan migrate --env=testting.docker
    
    chmod -R 777  database
    
    #create a single user
    php artisan user-app:register
```

Your app should work on your {IP/Domain}:{DOCKER_HOST_OUT_PORT}

#### _Refer the Command line section, to register a new user._




___
## Manual Installation notes:   
### PHP Dependencies
- Version: PHP 8.0

- Extensions
    - Sqlite3 Extension
    - OpenSSL PHP Extension
    - PDO PHP Extension
    - Mbstring PHP Extension
    - Tokenizer PHP Extension
    - XML PHP Extension
    - Ctype PHP Extension
    - JSON PHP Extension
    - BCMath PHP Extension
 


   
---
## Command Line Interface For The Application 
_(Artisan commands)_

#### To register a user, run the below command:
```
   > php artisan user-app:register
```

#### To reset a user's password, run the below command:
```
   > php artisan user-app:password-reset
```



___
## Testing the system


```shell    
./php-exec.sh
```

```shell    
# [if you have alreay run this, you can skip the below line.]
# Run the below, to create the additional testing database.
php artisan migrate --env=testting.docker

#Run the test casess.
php artisan test

```
