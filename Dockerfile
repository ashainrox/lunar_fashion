FROM phpdockerio/php80-fpm:latest

WORKDIR "/application"

RUN apt-get update; \
    apt-get -y --no-install-recommends install \
        git \
        php8.0-mysql; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

#For troubleshooting purpose
RUN apt-get update \
    && apt-get install -y \
    vim nano \
    php-sqlite3

#Adjust permissions to the project files
COPY ./ /application

RUN chown www-data:www-data -R /application

#laravel storage permissions
RUN chgrp -R www-data /application/storage /application/bootstrap/cache
RUN chmod -R ug+rwx  /application/storage  /application/bootstrap/cache

