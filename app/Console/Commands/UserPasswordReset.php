<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class UserPasswordReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user-app:password-reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User App Password Reset.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->ask('Email ?');
        $password = $this->secret('Password ?');
        $passwordConfirm = $this->secret('Password Confirmations?');

        $validator = Validator::make([
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $passwordConfirm,
        ], [
            'email' => 'required|email|exists:App\Models\User,email',
            'password' => 'required|between:6,15|confirmed',
        ]);

        if ($validator->fails()) {
            $this->warn('Something looks wrong. Check the error messages below:');

            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return 1;
        }else{
            $user = \App\Models\User::where('email', $email)->first();
            $user->password =  bcrypt($password);
            $user->save();

            //drop login sessions
            $user->tokens()->delete();

            $this->info('Password Updated. Use the API to login.');
            return 0;
        }

    }
}
