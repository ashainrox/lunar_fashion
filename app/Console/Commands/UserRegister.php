<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class UserRegister extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user-app:register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User App Registration.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->ask('Email ?');
        $password = $this->secret('Password ?');
        $passwordConfirm = $this->secret('Password Confirmations?');

        $validator = Validator::make([
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $passwordConfirm,
        ], [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|between:6,15|confirmed',
        ]);

        if ($validator->fails()) {
            $this->warn('Something looks wrong. Check the error messages below:');

            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return 1;
        }else{

            /** @var User $user */
            $user = User::create([
                'email' =>  $email,
                'password' => bcrypt($password),
                'name' =>  \Str::random(10)
            ]);

            $this->info('Account Created. Use the API to login.');

            return 0;
        }

    }
}
