<?php


namespace App\Components\Api;


class APIHttpConst
{
    const RESPONSE_CODES = [
        200 => 'success',
        204 => 'success',
        422 => 'failed',
    ];
}
