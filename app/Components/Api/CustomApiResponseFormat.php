<?php

namespace App\Components\Api;

trait CustomApiResponseFormat
{
    static function buildApiResponse($data = null, $responseCode=200, $message = null, $additionalData = []) {
        return [
                'data' => $data,
                'code' => $responseCode,
                'message' => is_null($message)?APIHttpConst::RESPONSE_CODES[$responseCode]:$message,
            ]+$additionalData;
    }
}
