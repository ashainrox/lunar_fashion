<?php


namespace App\Components\LunarConverter;


use Carbon\Carbon;

final class EarthToLunarTimeConverter extends MoonDateTime
{
    /**
     * Use Carbon instance (earth datetime)
     * @param Carbon $earthTimeStamp
     * @return static
     */
    public static function createFromEarthDateTime(Carbon $earthTimeStamp): self
    {
        $selfInstance = new self();

        //init properties
        $selfInstance->calcYears($earthTimeStamp);
        $selfInstance->calcDays($earthTimeStamp);
        $selfInstance->calcCycles($earthTimeStamp);
        $selfInstance->calcHours($earthTimeStamp);
        $selfInstance->calcMinutes($earthTimeStamp);
        $selfInstance->calcSeconds($earthTimeStamp);

        return $selfInstance;
    }

    private function __construct() {/*block creating instance directly*/}


    /**
     * This will return the difference in lunar seconds
     *      from origin-time to given-earth-time.
     *
     * @param Carbon $earthTimeStamp
     * @return int
     */
    private function getDiffFromOriginInSeconds(Carbon $earthTimeStamp): int{
        if( is_null( $this->timeInSeconds) ){

            //moonOriginAsInEarthTime
            $moonOriginTimeStamp = self::getOriginInEarthDateTime();

            $timeDifferenceInSeconds = $earthTimeStamp->copy()->diffInSeconds($moonOriginTimeStamp);

            $this->timeInSeconds = $timeDifferenceInSeconds/self::MOON_TO_EARTH_SECONDS_FACTOR;
        }

        return $this->timeInSeconds;
    }


    /**
     * Calculate & return the year component in the timestamp.
     *
     * @param Carbon $earthTimeStamp
     * @return int
     */
    private function calcYears(Carbon $earthTimeStamp): int
    {
        if( is_null($this->years) ){
            $this->years = (int)( $this->getDiffFromOriginInSeconds($earthTimeStamp) / self::YEARS_TO_SECONDS ) + 1;
        }

        return $this->years;
    }


    /**
     * Calculate & return the day component in the timestamp.
     *
     * @param Carbon $earthTimeStamp
     * @return int
     */
    private function calcDays(Carbon $earthTimeStamp): int
    {
        if( is_null($this->days) ){
            $this->days = (int)( ($this->getDiffFromOriginInSeconds($earthTimeStamp) % self::YEARS_TO_SECONDS) / self::DAYS_TO_SECONDS ) + 1;
        }

        return $this->days;
    }


    /**
     * Calculate & return the cycle component in the timestamp.
     *
     * @param Carbon $earthTimeStamp
     * @return int
     */
    private function calcCycles(Carbon $earthTimeStamp): int
    {
        if( is_null($this->cycles) ){
            $this->cycles = (int)( ($this->getDiffFromOriginInSeconds($earthTimeStamp) % self::DAYS_TO_SECONDS) / self::CYCLES_TO_SECONDS ) + 1;
        }

        return $this->cycles;
    }


    /**
     * Calculate & return the hour component in the timestamp.
     *
     * @param Carbon $earthTimeStamp
     * @return int
     */
    private function calcHours(Carbon $earthTimeStamp): int
    {
        if( is_null($this->hours) ){
            $this->hours = (int)( ($this->getDiffFromOriginInSeconds($earthTimeStamp) % self::CYCLES_TO_SECONDS) / self::HOURS_TO_SECONDS );
        }

        return $this->hours;
    }

    /**
     * Calculate & return the minute component in the timestamp.
     *
     * @param Carbon $earthTimeStamp
     * @return int
     */
    private function calcMinutes(Carbon $earthTimeStamp): int
    {
        if( is_null($this->minutes) ){
            $this->minutes = (int)( ($this->getDiffFromOriginInSeconds($earthTimeStamp) % self::HOURS_TO_SECONDS) / self::MINUTES_TO_SECONDS );
        }

        return $this->minutes;
    }

    /**
     * Calculate & return the second component in the timestamp.
     *
     * @param Carbon $earthTimeStamp
     * @return int
     */
    private function calcSeconds(Carbon $earthTimeStamp): int
    {
        if( is_null($this->seconds) ){
            $this->seconds = $this->getDiffFromOriginInSeconds($earthTimeStamp) % self::MINUTES_TO_SECONDS;
        }

        return $this->seconds;
    }




    /**
     * Calculate & return the timestamp in a string format.
     *      Example:- 52-12-29 ∇ 18:30:01
     *
     * @return string
     */
    public function toLunarDataTimeString(): string
    {
        return "{$this->years}-{$this->days}-{$this->cycles} ∇ {$this->hours}:{$this->minutes}:{$this->seconds}";
    }


    /**
     * Get the name of the day.
     * @return string
     */
    public function getDayName(): string
    {
        return self::DAY_NAMES[$this->days];
    }


    /**
     * Cast to string
     * @return string
     */
    public function __toString()
    {
       return $this->toLunarDataTimeString();
    }
}
