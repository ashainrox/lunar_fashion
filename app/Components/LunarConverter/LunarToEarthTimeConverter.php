<?php


namespace App\Components\LunarConverter;


use Carbon\Carbon;

final class LunarToEarthTimeConverter extends MoonDateTime
{
    /**
     * @param int $years
     * @param int $days
     * @param int $cycles
     * @param int $hours
     * @param int $minutes
     * @param int $seconds
     * @return LunarToEarthTimeConverter
     */
        public static function createFromLunarDateTime(int $years, int $days, int $cycles, int $hours, int $minutes, int $seconds): self
    {
        $selfInstance = new self();

        //assign values to properties
        $selfInstance->years = $years;
        $selfInstance->days = $days;
        $selfInstance->cycles = $cycles;
        $selfInstance->hours = $hours;
        $selfInstance->minutes = $minutes;
        $selfInstance->seconds = $seconds;

        /**
         * Calculate the total seconds since the lunar origin timestamp
         */
        $selfInstance->timeInSeconds =
            ( $selfInstance->seconds                                    ) +
            ( $selfInstance->minutes        * SELF::MINUTES_TO_SECONDS  ) +
            ( $selfInstance->hours          * SELF::HOURS_TO_SECONDS    ) +
            ( ($selfInstance->cycles -1)    * SELF::CYCLES_TO_SECONDS   ) +
            ( ($selfInstance->days   -1)    * SELF::DAYS_TO_SECONDS     ) +
            ( ($selfInstance->years  -1)    * SELF::YEARS_TO_SECONDS    );

        return $selfInstance;
    }

    private function __construct() {/*block creating instance directly*/}


    /**
     * @return Carbon
     */
    public function toEarthDataTime(): Carbon{
        /**
         * Convert lunar seconds to earth seconds
         */
        $secondsInEarthFormat = self::MOON_TO_EARTH_SECONDS_FACTOR * $this->timeInSeconds;


        /**
         * Add the time difference to the origin(in earth timestamp)
         */
        return self::getOriginInEarthDateTime()->addSeconds($secondsInEarthFormat);
    }


    /**
     * @param string $lunarDataTime
     *      Example:- 52-12-29 ∇ 18:30:01
     * @return LunarToEarthTimeConverter
     */
    public static function createFomDateTimeString(string $lunarDataTime): LunarToEarthTimeConverter
    {
        $tp = preg_split('/[-]|[ ]|[:]/', $lunarDataTime);
        return self::createFromLunarDateTime($tp[0], $tp[1], $tp[2], $tp[4], $tp[5], $tp[6]);
    }
}
