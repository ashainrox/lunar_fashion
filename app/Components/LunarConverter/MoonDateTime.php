<?php


namespace App\Components\LunarConverter;


use Carbon\Carbon;

abstract class MoonDateTime
{
    protected const YEARS_TO_SECONDS      = 12 * self::DAYS_TO_SECONDS;
    protected const DAYS_TO_SECONDS       = 30 * self::CYCLES_TO_SECONDS;
    protected const CYCLES_TO_SECONDS     = 24 * self::HOURS_TO_SECONDS;
    protected const HOURS_TO_SECONDS      = 60 * self::MINUTES_TO_SECONDS;
    protected const MINUTES_TO_SECONDS    = 60;

    protected const MOON_TO_EARTH_SECONDS_FACTOR = 29.530589/30;

    public const DAY_NAMES = [
        1  => 'Armstrong',
        2  => 'Aldrin',
        3  => 'Conrad',
        4  => 'Bean',
        5  => 'Shepard',
        6  => 'Mitchell',
        7  => 'Scott',
        8  => 'Irwin',
        9  => 'Young',
        10 => 'Duke',
        11 => 'Duke',
        12 => 'Schmitt',
    ];

    protected $seconds    = null;
    protected $minutes    = null;
    protected $hours      = null;
    protected $cycles     = null;
    protected $days       = null;
    protected $years      = null;

    protected $timeInSeconds = null;

    /**
     * Neil Armstrong set foot on the Moon surface on July 21th 1969 at 02:56:15 UT,
     *  and this is the point in time for the calendar to start for the moon.
     * @return Carbon
     */
    public static function getOriginInEarthDateTime(): Carbon
    {
        return Carbon::create(1969, 7, 21, 02,56,15)->timezone('UTC');
    }

}
