<?php

function apiResponse($data = null, $responseCode=200, $message = null, $additionalData = [], $headers =  []){
    return response()->json(\App\Components\Api\CustomApiResponseFormat::buildApiResponse($data, $responseCode, $message, $additionalData), $responseCode, $headers);
}
