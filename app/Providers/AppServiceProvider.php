<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * If database drive is sqlite and
         *  sqlite database does not exist,
         *      then create a blank one.
        */
        if( config('database.default') == 'sqlite'){
            $dbPath = config('database.connections.sqlite.database');
            if (!file_exists($dbPath)) {
                touch($dbPath);
            }

            $dbPath = config('database.connections.sqlite_testing.database');
            if (!file_exists($dbPath)) {
                touch($dbPath);
            }
        }

    }
}
