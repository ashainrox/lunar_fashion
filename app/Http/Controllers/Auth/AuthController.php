<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Login api
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(\Auth::attempt(['email' => $request->email, 'password' => $request->password])){

            $user = \Auth::user();
            $responseData['token'] =  $user->createToken('VC-LF')->plainTextToken;
            $responseData['user'] =  $user;

            return apiResponse($responseData);
        }

        abort(401, 'Invalid Credentials.');
    }


    /**
     * Login api
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        auth()->user()->tokens()->delete();

        return apiResponse("Logged Out");
    }


    /**
     * Update user password.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function resetPassword(Request $request): JsonResponse
    {
        $input = $request->validate([
            'old_password'          => 'required|current_password',
            'password'              => 'required|between:6,15|confirmed',
        ]);

        if (\Hash::check($input['old_password'], auth()->user()->password)) {
            $user = auth()->user();
            $user->password = bcrypt($input['password']);
            $user->save();

            $user->tokens()->delete();

            return apiResponse("Password Updated");
        }
    }
}
