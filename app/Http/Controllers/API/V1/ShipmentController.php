<?php

namespace App\Http\Controllers\API\V1;

use App\Components\LunarConverter\EarthToLunarTimeConverter;
use App\Components\LunarConverter\LunarToEarthTimeConverter;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShipmentController extends Controller
{
    public function getArrivalTime(Request $request): JsonResponse
    {
        $request->validate([
            'shipment_left_at' => 'required|date_format:Y-m-d H:i:s',
        ]);


        /**
         * `time of departure` in UTC,  as a Carbon::class instance.
         */
        //Create (Carbon) datetime object in UTC
        $warehouseLeftAt = Carbon::createFromTimeString( $request->get('shipment_left_at') )->timezone('UTC');


        /**
         * Taken from SRS.
         * Time taken from warehouse to luna colony.
         */
        $travelTimeInDays = 3; //3 days


        /**
         * `time of arrival` in UTC,  as a Carbon::class instance.
         */
        $timeOfArrivalInUTC = $warehouseLeftAt->addDays($travelTimeInDays);


        /**
         * `time of arrival` in LST, as a EarthToLunarTimeConverter::class instance
         */
        $timeOfArrivalInLST = EarthToLunarTimeConverter::createFromEarthDateTime($timeOfArrivalInUTC);


        //prepare response data
        $data = [
            'lst_arrival_to_colony' => (string) $timeOfArrivalInLST,
            'lst_day_name'          => $timeOfArrivalInLST->getDayName(),
            'utc_arrival_to_colony' => $timeOfArrivalInUTC->format('Y-m-d H:i:s'),
        ];


        //Reverse Calculation [ if needed ].
        /**
         *      LunarToEarthTimeConverter::createFromLunarDateTime(52, 12, 29, 18, 30, 1)
         *          ->toEarthDataTime()->toString();
         *
         *      LunarToEarthTimeConverter::createFomDateTimeString('52-12-29 ∇ 18:30:01')
         *          ->toEarthDataTime()->toString();
         */

        return apiResponse(($data));
    }
}
