<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class ShipmentApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_shipment_authentication_rejection()
    {
        $data = [
          'shipment_left_at' => '2021-09-23 01:01:01',
        ];
        $response = $this->getJson('api/v1/shipment/arrival-time', $data );

        $response->assertStatus(401);
    }


    public function test_shipment_authentication_acceptance()
    {
        $data = [
            'shipment_left_at' => '2021-09-23 01:01:01',
        ];

        $headers = [
            'Authorization' => 'Bearer '.User::factory()->create()->createToken('VC-LF')->plainTextToken,
        ];

        $this->json('GET', 'api/v1/shipment/arrival-time', $data, $headers )
            ->assertStatus(200);
    }


    public function test_shipment_return_time_validity()
    {
        $data = [
            'shipment_left_at' => '2021-09-23 01:01:01',
        ];

        $headers = [
            'Authorization' => 'Bearer '.User::factory()->create()->createToken('VC-LF')->plainTextToken,
        ];

        $response = $this->json('GET', 'api/v1/shipment/arrival-time', $data, $headers );

        /**
         * time is manually calculated and added here, to check against the programmatically calculated time
        */
        $response->assertJsonPath('data.lst_arrival_to_colony', "54-10-13 ∇ 21:24:10");
    }
}
