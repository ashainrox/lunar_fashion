<?php

namespace Tests\Unit;

use App\Components\LunarConverter\EarthToLunarTimeConverter;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class TImeConversionTest extends TestCase
{
    public function test_earth_to_lunar_time_conversion()
    {
        $earthToLunarConverter = EarthToLunarTimeConverter::createFromEarthDateTime(new Carbon('2021-09-23 01:01:01'));

        /**
         * time is ten manually from `http://lunarclock.org/convert-to-lunar-standard-time.php` and added here,
         *  to check against the programmatically calculated time
         */
        $timeComparisonStatus = $earthToLunarConverter->toLunarDataTimeString() == "54-10-10 ∇ 20:15:30";
        $this->assertTrue($timeComparisonStatus);
    }
}
