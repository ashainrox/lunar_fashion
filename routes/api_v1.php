<?php

use App\Http\Controllers\API\V1\ShipmentController;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Protected Routes
Route::group(['middleware' => 'auth:sanctum'], function()
{
    Route::get('shipment/arrival-time',  [ShipmentController::class, 'getArrivalTime'])->name('logout.api');
});



