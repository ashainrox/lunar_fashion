<?php

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Login
Route::post('login', [AuthController::class, 'login'])->name('login.api');

//Protected Routes
Route::group(['middleware' => 'auth:sanctum'], function()
{
    Route::post('logout',  [AuthController::class, 'logout'])->name('logout.api');
    Route::post('password-reset',  [AuthController::class, 'resetPassword'])->name('password-reset.api');
});


